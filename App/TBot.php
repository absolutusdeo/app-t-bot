<?php
require_once 'file/type.php';
require_once 'file/file.php';

class TBot extends File{
    protected $data;
    protected $message;
    protected $file;
    public $chatId;
    protected $type;

    public function __construct($data,$token){
        parent::__construct($token);
        $this->data = $data;
        $this->chatId = $data['message']['chat']['id'];
        $this->message = $data['message'];
        $this->file = array_pop($data['message']);
        $this->type = Type::typeObject($this->data);
    }
/**
 *Ф-ція обробки даних класу отриманих від користувача.
 * @patam json $data
 */
    public function debugTBot(){
        ob_start();
        print_r($this->data);
        $out = ob_get_clean();
        file_put_contents(__DIR__ . '/debug.txt', $out);
        ob_clean();
    }

/**
 * відправлення текстового повідомлення
 * @param string $text
 * @param $chat_id
 */
    public function sendMessage(string $text, $chat_id){
        $this->sendTelegram(
            'sendMessage',
            array(
                'chat_id' => trim($chat_id),
                'text' => $text,
            )
        );
    }

    public function main(){
        if(!empty($this->type)){
            $this->saveFile($this->type, $this->message, $this->chatId);
        }else{
            $this->sendMessage('ERROR TYPE', $this->chatId);
        }
    }
}