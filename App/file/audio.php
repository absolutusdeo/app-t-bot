<?php

class Audio implements file {

    public function save($data){
        $original_photo = $this->file;
        
        $res = self::sendTelegram(
            'getFile',
            array(
                'file_id' => $original_photo['file_id']
            )
        );

        $res = json_decode($res, true);
        if($res['ok']){
            $src = 'https://api.telegram.org/file/bot' . TOKEN . '/' . $res['result']['file_path'];
            $dest = __DIR__ . '/' . time() . '-' . basename($src);

            if(copy($src, $dest)){
                self::sendTelegram(
                    'sendMessage',
                    array(
                        'chat_id' => $this->chatId,
                        'text' => 'photo save',
                    )
                );

            }
        }

        return true;
    }

}