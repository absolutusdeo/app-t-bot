<?php
class File{
    protected $token;
    public function __construct($token){
        $this->token = $token;
    }

    /**
     * ������� ��� ������ � api-telegram
     */
    public function sendTelegram($method, $response){
        $ch = curl_init('https://api.telegram.org/bot' . $this->token . '/' . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);

        return $res;
    }


    protected function dirMaker($type){
        mkdir($_SERVER['DOCUMENT_ROOT']  . '/' . "bot". '/' . "archive" . '/' .$type.'/');
    }

    protected function saveFile($type, $message, $chat_id){
        //�������� �� ���
        if($type == 'photo'){
            $file_final = array_pop($message[$type]);
        }else{
            $file_final = $message[$type];
        }



        // ��������� ����� ����� � ���� ���������
        $res = $this->sendTelegram(
            'getFile',
            array(
                'file_id' => $file_final['file_id']
            )
        );

        $res = json_decode($res, true);
        if($res['ok']){
            $src = 'https://api.telegram.org/file/bot' . $this->token . '/' . $res['result']['file_path'];
            $path = $_SERVER['DOCUMENT_ROOT']  . '/' . "bot". '/' . "archive" . '/' . $type . '/'. $chat_id . '/';
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            };
            $dest =  $path . time() . '-'  . basename($src);


            if(copy($src, $dest)){ //�������� �� ������������ ����� �� ������ � ����������� ��� ������ ������������
                $this->sendTelegram(
                    'sendMessage',
                    array(
                        'chat_id' => $chat_id,
                        'text' => $type.' save',
                    )
                );

            }
        }

        exit();
    }
}